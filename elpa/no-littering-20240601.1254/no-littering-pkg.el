(define-package "no-littering" "20240601.1254" "Help keeping ~/.config/emacs clean"
  '((emacs "25.1")
    (compat "29.1.4.2"))
  :commit "4a71c736ecf04f9b8e5bdd10a73ef1febb73a557" :authors
  '(("Jonas Bernoulli" . "emacs.no-littering@jonas.bernoulli.dev"))
  :maintainers
  '(("Jonas Bernoulli" . "emacs.no-littering@jonas.bernoulli.dev"))
  :maintainer
  '("Jonas Bernoulli" . "emacs.no-littering@jonas.bernoulli.dev")
  :keywords
  '("convenience")
  :url "https://github.com/emacscollective/no-littering")
;; Local Variables:
;; no-byte-compile: t
;; End:
