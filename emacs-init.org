* Base settings
** Always rebuild init

Emacs expands this file into an elisp file. The elisp file contains the code at the time it's created. So it may have redundant code.

The following code block ensures that a new elisp file is created from this org mode file on startup.

#+BEGIN_SRC emacs-lisp
  (use-package emacs
    :config
    (defun so/delete-emacs-init ()
      (interactive)
      (let ((configs "C:/emacs/.emacs.d/emacs-init.el"))
	(when configs
	  (delete-file configs))))
    :hook ((kill-emacs-hook . so/delete-emacs-init)
	   ;; Speed up loading of packages
	   (kill-emacs-hook . package-quickstart-refresh)))
#+END_SRC
** Put custom settings in a separate file
 #+BEGIN_SRC emacs-lisp
   (use-package cus-edit
     :config
     (setq custom-file "C:/emacs/.emacs.d/custom.el")

     ;; The custom file setting can conflict with other settings so erase it onstartup
     (unless (file-exists-p custom-file)
       (write-region "" nil custom-file))

     (load custom-file))
 #+END_SRC
** Startup performance
#+BEGIN_SRC emacs-lisp
  (setq gc-cons-threshold (* 50 1000 1000))
#+END_SRC
** Keep folders clean
#+BEGIN_SRC emacs-lisp
  (use-package no-littering
    :ensure t)

  ;; no-littering doesn't set this by default so we must place auto save files in the same path as it uses for sessions
  (setq auto-save-file-name-transforms
	`((".*" ,(no-littering-expand-var-file-name "auto-save/") t)))

  ;; Put all backup files in one directory and use version control
  (setq
   make-backup-files t
   backup-by-copying t
   backup-directory-alist
   '(("." . "C:/emacs/.emacs.d/emacs_backup_files"))
   delete-old-versions t
   kept-new-versions 6
   kept-old-versions 2
   version-control t)
#+END_SRC
** Avoid using outdated byte-compiled packages
#+BEGIN_SRC emacs-lisp
  (setq load-prefer-newer t)
#+END_SRC
** Start Emacs server
#+BEGIN_SRC emacs-lisp
  (server-start)
#+END_SRC
** Async
 #+BEGIN_SRC emacs-lisp
   (use-package async
     :ensure t
     :config
     (autoload 'dired-async-mode "dired-async.el" nil t)
     (dired-async-mode 1))
 #+END_SRC
* UI
** GUI
 #+BEGIN_SRC emacs-lisp
   (use-package emacs
     :init
     (tool-bar-mode -1)
     (menu-bar-mode -1)
     (scroll-bar-mode -1)
     :config
     (setq inhibit-splash-screen t)
     ;; Font
     (set-face-attribute 'default nil
			 :family "Source Code Pro"
			 :height 140
			 :weight 'normal
			 :width 'normal))
 #+END_SRC
** Modeline
   Reduce minor mode info in modeline.
#+BEGIN_SRC emacs-lisp
  (use-package doom-modeline
    :ensure t
    :config (doom-modeline-mode 1)
    :custom ((doom-modeline-height 15)))
#+END_SRC
** Theme
 #+BEGIN_SRC emacs-lisp
   (load-theme 'modus-vivendi t)
 #+END_SRC
** Transparency
 #+BEGIN_SRC emacs-lisp
   (set-frame-parameter (selected-frame) 'alpha '(85 . 50))
   (add-to-list 'default-frame-alist `(alpha . ,'(85 . 50)))
   (set-frame-parameter (selected-frame) 'fullscreen 'maximized)
   (add-to-list 'default-frame-alist '(fullscreen . maximized))
 #+END_SRC
** Highlight colour words and hex codes
 #+BEGIN_SRC emacs-lisp
   (use-package rainbow-mode
     :ensure t
     :hook
     (prog-mode-hook . rainbow-mode))
 #+END_SRC
** Misc
#+BEGIN_SRC emacs-lisp
  ;; Scroll this many lines when point moves off screen
  (setq scroll-conservatively 1)
  ;; Keep this many lines above or below the cursor
  (setq scroll-margin 7)
  ;; Highlight current line
  (global-hl-line-mode)
  ;; Wrap lines
  (global-visual-line-mode 1)
#+END_SRC
* Text editing
** Vim bindings
*** General vim bindings
 #+BEGIN_SRC emacs-lisp
   ;; Vim bindings in the text editor
   (use-package evil
     :ensure t
     :init
     (setq evil-want-keybinding nil)
     (setq evil-want-minibuffer t)
     ; Disable C-i binding in terminal
     (if (display-graphic-p) nil
       (setq evil-want-C-i-jump nil))
     :config
     (evil-mode 1))

   ;; C-i doesn't work in a terminal. Rebind it to C-t
   (if (display-graphic-p) nil
     (use-package evil
       :bind (:map evil-normal-state-map
	      ("C-t" . evil-jump-forward))))

   ;; Vim bindings nearly everywhere
   (use-package evil-collection
     :after evil
     :ensure t
     :config
     (evil-collection-init))

   ;; Use jj to exit insert mode
   (use-package key-chord
     :ensure t
     :after evil
     :config
     ;; Max time delay between two key presses to be considered a key chord
     (setq key-chord-two-keys-delay 1)
     ;; Max time delay between two presses of the same key to be considered a key
     ;; chord.
     ;; Should normally be a little longer than `key-chord-two-keys-delay'.
     (setq key-chord-one-key-delay 1.1)
     ;; Exit insert mode by pressing j and then j quickly
     (key-chord-define evil-insert-state-map "jj" 'evil-normal-state)
     (key-chord-mode 1))

   ;; Vim bindings in org mode
   (use-package evil-org
     :ensure t
     :after (org evil)
     :config
     (evil-org-set-key-theme '(navigation insert textobjects additional calendar))
     :hook
     (org-mode-hook . evil-org-mode))
 #+END_SRC
*** Leader key
 #+BEGIN_SRC emacs-lisp
   (use-package evil-leader
     :after evil
     :ensure t
     :config
     (global-evil-leader-mode)
     (evil-leader/set-leader "<SPC>"))
 #+END_SRC
*** Change surrounding characters in pairs
 #+BEGIN_SRC emacs-lisp
   (use-package evil-surround
     :ensure t
     :after evil
     :config
     (global-evil-surround-mode 1))
 #+END_SRC
** Vim like undo
 #+BEGIN_SRC emacs-lisp
   (use-package undo-tree
     :ensure t
     :config
     (setq undo-tree-history-directory-alist '(("." . "C:/emacs/.emacs.d/undo")))
     (global-undo-tree-mode)
     (evil-set-undo-system 'undo-tree))
 #+END_SRC
** Distraction free editing
 #+BEGIN_SRC emacs-lisp
   (use-package writeroom-mode
     :ensure t
     :commands writeroom-mode
     :config
     ; Keep transparency
     ; Don't set fullscreen
     (setq writeroom-global-effects
	   '(writeroom-set-menu-bar-lines
	     writeroom-set-tool-bar-lines
	     writeroom-set-vertical-scroll-bars
	     writeroom-set-bottom-divider-width)))
 #+END_SRC
** Jump to things
 #+BEGIN_SRC emacs-lisp
   (use-package avy
     :ensure t
     :defer 0)
 #+END_SRC
** Misc
#+BEGIN_SRC emacs-lisp
  ;; Auto reload externally changed file
  (global-auto-revert-mode 1)
  ;; Auto update Dired and other buffers when changes are made externally
  (setq global-auto-revert-non-file-buffers t)

  ;; Use hunspell dictionary
  (setq ispell-program-name "hunspell")
  (setq ispell-hunspell-dict-paths-alist '(("en_GB" "C:/hunspell/share/hunspell/en_GB.aff")))
  (setq ispell-local-dictionary "en_GB")
  (setq ispell-local-dictionary-alist '(("en_GB" "[[:alpha:]]" "[^[:alpha:]]" "[']" nil ("-d" "en_GB") nil utf-8)))
  ;; Enable spell checking
  (add-hook 'text-mode-hook #'flyspell-mode)

  ;; Enable narrowing
  (put 'narrow-to-region 'disabled nil)

  ;; Enable function to erase a buffer
  (put 'erase-buffer 'disabled nil)

  ;; Delete trailing whitespace and lines
  (add-hook 'before-save-hook 'delete-trailing-whitespace)
  (defun so/delete-trailing-blank-lines ()
    (interactive)
    (save-excursion
      (save-restriction
	(widen)
	(goto-char (point-max))
	(delete-blank-lines)
	(let ((trailnewlines (abs (skip-chars-backward "\n\t"))))
	  (if (> trailnewlines 0)
	      (progn
		(delete-char trailnewlines)))))))
  (add-hook 'before-save-hook 'so/delete-trailing-blank-lines)
#+END_SRC
* Completion
** Text completion
 #+BEGIN_SRC emacs-lisp
   ;; Completion framework
   (use-package company
     :ensure t
     :defer 0
     :config
     (setq company-idle-delay 0.0
	   company-minimum-prefix-length 1
	   company-selection-wrap-around t)
     (define-key company-active-map (kbd "RET") nil)
     (define-key company-active-map (kbd "<return>") nil)
     (define-key company-active-map (kbd "<return>") nil)
     (define-key company-active-map (kbd "<C-tab>") 'company-complete-selection)
     (global-company-mode))

   ;; Read more data
   (setq read-process-output-max (* 1024 1024))
 #+END_SRC
** General completion
 #+BEGIN_SRC emacs-lisp
   ;; Completion framework
   ;; This automatically installs swiper, an incremental search that uses ivy
   (use-package ivy
     :ensure t
     :diminish
     :bind (("C-s" . swiper)
     :map ivy-minibuffer-map
     ("TAB" . ivy-alt-done)
     ("C-l" . ivy-alt-done)
     ("C-j" . ivy-next-line)
     ("C-k" . ivy-previous-line)
     :map ivy-switch-buffer-map
     ("C-k" . ivy-previous-line)
     ("C-l" . ivy-done)
     ("C-d" . ivy-switch-buffer-kill)
     :map ivy-reverse-i-search-map
     ("C-k" . ivy-previous-line)
     ("C-d" . ivy-reverse-i-search-kill))
     :config
     (ivy-mode 1)
     (setq ivy-re-builders-alist '((t . ivy--regex-fuzzy)))
     (setq ivy-initial-inputs-alist nil))

   ;; Various small improvements to ivy
   (use-package ivy-rich
     :ensure t
     :after ivy
     :config
     (ivy-rich-mode 1))

   ;; Replace some emacs commands with equivalents that utilise ivy.
   (use-package counsel
     :ensure t
     :bind (("C-M-j" . 'counsel-switch-buffer)
     :map minibuffer-local-map
     ("C-r" . 'counsel-minibuffer-history))
     :custom
     (counsel-linux-app-format-function #'counsel-linux-app-format-function-name-only)
     :config
     (counsel-mode 1))

   ;; Improve ivy completion ordering
   (use-package ivy-prescient
     :ensure t
     :after counsel
     :custom
     (ivy-prescient-enable-filtering nil)
     :config
     ;; Remember sorting across sessions
     (prescient-persist-mode 1)
     (ivy-prescient-mode 1))
 #+END_SRC
* Programming
** Linting
 #+BEGIN_SRC emacs-lisp
   (use-package flycheck
     :ensure t
     :hook
     (prog-mode-hook . flycheck-mode))
 #+END_SRC
** Brackets/delimiters
*** Highlight delimiters, using different colours for nested delimiters
 #+BEGIN_SRC emacs-lisp
   (use-package rainbow-delimiters
     :ensure t
     :hook
     (prog-mode-hook . rainbow-delimiters-mode))
 #+END_SRC
*** Highlight the bracket matching the one at the point
 #+BEGIN_SRC emacs-lisp
   (show-paren-mode 'parenthesis)
 #+END_SRC
** Comments
 #+BEGIN_SRC emacs-lisp
   (use-package evil-commentary
     :after evil
     :ensure t
     :config
     (evil-commentary-mode))
 #+END_SRC
** Misc
#+BEGIN_SRC emacs-lisp
  ;; Automatic indentation
  (electric-indent-mode 1)
  ;; Insert brackets and double quotes in pairs
  (add-hook 'prog-mode-hook #'electric-pair-mode)
  ;; Spell check code comments
  (add-hook 'prog-mode-hook #'flyspell-prog-mode)
  ;; Prevent ediff from spawning a new frame
  (setq ediff-window-setup-function 'ediff-setup-windows-plain)
#+END_SRC
* Dired
#+BEGIN_SRC emacs-lisp
  (use-package dired
    :config
    ;; Dired will try to guess the default folder
    (setq dired-dwim-target t)
    (setq delete-by-moving-to-trash t)
    (setq dired-autorevert-buffer t)
    ;; Enable reusing the same buffer in dired
    (put 'dired-find-alternate-file 'disabled nil)
    ;; Flags to pass to ls
    (setq dired-listing-switches "-AFhl --group-directories-first --time-style=long-iso")
    :hook (dired-mode-hook . dired-hide-details-mode))
#+END_SRC
** Make dired reuse buffers
 #+BEGIN_SRC emacs-lisp
   (use-package dired-single
     :ensure t
     :after dired)

   (with-eval-after-load 'dired
       (defun so/dired-init ()
	 "Bunch of stuff to run for dired, either immediately or when it's loaded."
	 (define-key dired-mode-map [remap dired-find-file]
	   'dired-single-buffer)
	 (define-key dired-mode-map [remap dired-mouse-find-file-other-window]
	   'dired-single-buffer-mouse)
	 (define-key dired-mode-map [remap dired-up-directory]
	   'dired-single-up-directory))

     ;; If dired's already loaded, then the keymap will be bound
     (if (boundp 'dired-mode-map)
	 ;; It's loaded, so add our bindings
	 (so/dired-init)
       ;; It's not loaded yet, so add our bindings to the load-hook
       (add-hook 'dired-load-hook 'so/dired-init)))
 #+END_SRC
* Notes
** Org roam
#+BEGIN_SRC emacs-lisp
  (use-package org-roam
    :ensure t
    :custom
    (org-roam-directory "C:/Users/Suraj/Documents/Notes")
    :config
    (org-roam-db-autosync-enable))
#+END_SRC
* Git
** Magit
 #+BEGIN_SRC emacs-lisp
   (use-package magit
     :ensure t
     :defer 0
     :commands magit-status
     :custom
     (magit-display-buffer-function #'magit-display-buffer-same-window-except-diff-v1))
 #+END_SRC
** Git gutter
 #+BEGIN_SRC emacs-lisp
   (use-package git-gutter
     :ensure t
     :config
     (global-git-gutter-mode 1))
 #+END_SRC
* Terminal
** Eshell
 #+BEGIN_SRC emacs-lisp
   (use-package esh-autosuggest
     :ensure t
     :hook (eshell-mode-hook . esh-autosuggest-mode))

   ;; Create eshell aliases
   (setq so/eshell-aliases
	 '((l . (lambda () (eshell/ls '-lhA)))
	   (replaygain . (lambda () (eshell-command "mp3gain -r -c *.mp3")))
	   (yda . (lambda (&rest args)
		    (if (null args)
			(message "Please provide a URL")
		      (dolist (url args)
			(async-shell-command (format "yt-dlp --continue --ignore-errors --extract-audio --audio-format mp3 -o %%(title)s.%%(ext)s %s" url))))))))

   ;; Load eshell aliases
   (mapc (lambda (alias)
	   (defalias (car alias) (cdr alias)))
	 so/eshell-aliases)
 #+END_SRC
** PowerShell
 #+BEGIN_SRC emacs-lisp
   (use-package powershell
     :ensure t)
 #+END_SRC
* Misc
** Quickly switch buffers
#+BEGIN_SRC emacs-lisp
  (use-package ace-jump-buffer :ensure t)
#+END_SRC
** Temporary mode with different bindings
#+BEGIN_SRC emacs-lisp
  (use-package hydra
    :ensure t)
#+END_SRC
** Edit the result of a grep search
#+BEGIN_SRC emacs-lisp
  (use-package wgrep
    :ensure t
    :commands wgrep
    :config
    (setq wgrep-auto-save-buffer t)
    (setq wgrep-change-readonly-file t))
#+END_SRC
** Multi-occur project/dired files
#+BEGIN_SRC emacs-lisp
  (use-package noccur
    :ensure t)
#+END_SRC
** Package update
#+BEGIN_SRC emacs-lisp
  (use-package paradox
    :ensure t
    :commands paradox-list-packages
    :config
    (setq paradox-execute-asynchronously t))
#+END_SRC
** Restart emacs
#+BEGIN_SRC emacs-lisp
  (use-package restart-emacs
    :ensure t)
#+END_SRC
** Regexp with visual feedback
#+begin_src emacs-lisp
  (use-package visual-regexp :ensure t)
#+end_src
** Improve emacs help functions
#+BEGIN_SRC emacs-lisp
  (use-package helpful
    :ensure t
    :commands (helpful-callable helpful-variable helpful-command helpful-key)
    :custom
    (counsel-describe-function-function #'helpful-callable)
    (counsel-describe-variable-function #'helpful-variable)
    :bind
    ([remap describe-function] . counsel-describe-function)
    ([remap describe-command] . helpful-command)
    ([remap describe-variable] . counsel-describe-variable)
    ([remap describe-key] . helpful-key))
#+END_SRC
** Move buffers between windows
 #+BEGIN_SRC emacs-lisp
   (use-package buffer-move
     :ensure t)
 #+END_SRC
** Recent files
#+BEGIN_SRC emacs-lisp
  ;; Keep track of opened files
  (recentf-mode 1)
  (setq recentf-max-menu-items 100)
  (setq recentf-max-saved-items 100)
  ;; Remove recent files that no longer exist
  (recentf-cleanup)
  ;; Save recent files periodically
  (async-start
   (run-at-time t (* 30 60) 'recentf-save-list))
#+END_SRC
** Custom functions
 #+BEGIN_SRC emacs-lisp
   (defun so/switch-or-create-scratch-buffer nil
     (interactive)
     (switch-to-buffer "*scratch*")
     (lisp-interaction-mode))

   (defun so/evil-window-increase-height ()
     (interactive)
     (evil-window-increase-height 5))

   (defun so/evil-window-decrease-height ()
     (interactive)
     (evil-window-decrease-height 5))

   (defun so/evil-window-decrease-width ()
     (interactive)
     (evil-window-decrease-width 10))

   (defun so/evil-window-increase-width ()
     (interactive)
     (evil-window-increase-width 10))

   (defhydra hydra-text-scale ()
     ("i" text-scale-increase "in")
     ("o" text-scale-decrease "out")
     ("r" text-scale-set "reset")
     ("f" nil "finished" :exit t))
 #+END_SRC
* Key bindings
** Display a popup showing key chords
#+BEGIN_SRC emacs-lisp
  (use-package which-key
    :ensure t
    :init
    (setq which-key-enable-extended-define-key t)
    :config
    (setq which-key-allow-evil-operators t)
    (which-key-mode)
    (which-key-add-key-based-replacements
      "<SPC> b" "buffers"
      "<SPC> c" "code"
      "<SPC> f" "files"
      "<SPC> g" "git"
      "<SPC> h" "help"
      "<SPC> j" "avy"
      "<SPC> l" "flyspell"
      "<SPC> m" "bookmarks"
      "<SPC> n" "notes"
      "<SPC> o" "other"
      "<SPC> s" "search"
      "<SPC> t" "terminal"
      "<SPC> w" "window"
      "<SPC> z" "zoom"))
#+END_SRC
** Evil leader bindings
   The functions that are bound to need to be defined before this. So this must go at the end.
  #+BEGIN_SRC emacs-lisp
    (evil-leader/set-key
      "<SPC>" 'counsel-M-x
      ";" 'eval-last-sexp

      "bb" 'counsel-switch-buffer
      "bp" 'previous-buffer
      "bn" 'next-buffer
      "bd" '(lambda () (interactive) (kill-buffer (current-buffer)))
      "bs" 'save-buffer
      "br" 'narrow-to-region
      "bw" 'widen

      "cc" 'evil-commentary-line
      "cm" 'evil-commentary
      "cn" 'next-error
      "cp" 'previous-error
      "cx" 'list-flycheck-errors

      "fd" 'counsel-dired
      "fe" 'counsel-grep
      "ff" 'counsel-find-file
      "fg" 'counsel-git
      "fh" 'counsel-git-grep
      "fr" 'counsel-recentf
      "fs" 'swiper
      "fu" 'counsel-file-jump

      "gg" 'magit-status
      "gu" 'git-gutter
      "g=" 'git-gutter:popup-hunk
      "gp" 'git-gutter:previous-hunk
      "gn" 'git-gutter:next-hunk
      "gs" 'git-gutter:stage-hunk

      ;; Help
      "h" (lookup-key global-map (kbd "C-h"))

      "jc" 'avy-goto-char
      "jw" 'avy-goto-word-1
      "jl" 'avy-goto-line
      "jm" 'avy-linum-mode
      "jj" 'ace-jump-buffer

      "mc" 'bookmark-set-no-overwrite
      "mC" 'bookmark-set
      "mj" 'bookmark-jump
      "ml" 'list-bookmarks
      "md" 'bookmark-delete

      ;; Zettelkasten notes
      "nb" 'org-roam-buffer-toggle
      "nf" 'org-roam-node-find
      "ni" 'org-roam-node-insert
      "nn" 'org-roam-capture

      "os" 'so/switch-or-create-scratch-buffer
      "ot" 'counsel-load-theme
      "ou" 'undo-tree-visualize

      "te" 'eshell
      "tp" 'powershell

      "w+" 'so/evil-window-increase-height
      "w-" 'so/evil-window-decrease-height
      "w<" 'so/evil-window-decrease-width
      "w>" 'so/evil-window-increase-width
      "w=" 'balance-windows
      "wh" 'windmove-left
      "wl" 'windmove-right
      "wk" 'windmove-up
      "wj" 'windmove-down
      "wH" 'buf-move-left
      "wL" 'buf-move-right
      "wK" 'buf-move-up
      "wJ" 'buf-move-down
      "wd" 'evil-window-delete
      "ws" 'evil-window-split
      "wv" 'evil-window-vsplit
      "wo" 'delete-other-windows

      "z" '(lambda () (interactive) (hydra-text-scale/body)))
  #+END_SRC
** Other key bindings
#+BEGIN_SRC emacs-lisp
  (global-set-key [f7] 'browse-url-of-file)
#+END_SRC
* Todo
#+BEGIN_SRC emacs-lisp
  (setq org-hierarchical-todo-statistics nil)
#+END_SRC
* Runtime performance
#+BEGIN_SRC emacs-lisp
  (setq gc-cons-threshold (* 2 1000 1000))
#+END_SRC